<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/21/20
  Time: 1:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My Products WishList</title>
    <%@include file="/partials/bootstrap.jsp"%>
</head>
<body>
<%@include file="/partials/navbar.jsp"%>

    <center>
        <h1>Welcome to my Product WishList Page</h1>
            <br>
        <h3>Here are all of the product's that are on my wishlist</h3>
            <br>
        <table border="1">
            <tr>
                <th>Name</th>
                <th>Category</th>
                <th>Price</th>
            </tr>

            <c:forEach items="${listOfProducts}" var="displayedProducts">
                <tr>
                    <td>${displayedProducts.name}</td>
                    <td>${displayedProducts.category}</td>
                    <td>${displayedProducts.price}</td>
                </tr>
            </c:forEach>
        </table>
        <a href="/products/add-product">Want to add a new product?</a>
    </center>
    <%@include file="/partials/footer.jsp"%>
</body>
</html>
