<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/20/20
  Time: 3:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
    <%@include file="/partials/bootstrap.jsp"%>
</head>
<body>
<%@include file="/partials/navbar.jsp"%>
    <center>
        <br>
        <br>
        <br>
        <h1>
            Some Error has occurred, Please try again
        </h1>

        <a href="login.jsp">Return to login page</a>
    </center>
<%@include file="/partials/footer.jsp"%>
</body>
</html>
