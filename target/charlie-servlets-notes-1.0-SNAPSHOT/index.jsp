<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/20/20
  Time: 10:28 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Java EE Lesson</title>
<%--    include bootstrap --%>
    <%@include file="partials/bootstrap.jsp"%>
    <style>
      .card {
        position: fixed;
      }
    </style>
  </head>
  <body>
<%--  Navbar included--%>
<%@include file="partials/navbar.jsp"%>

  <center>
    <h1>Welcome to my Java EE Application</h1>
      <br>
      <br>
    <div class="container">

      <div class="row">

        <div class="card col-6" style="width: 18rem;">
          <div class="card-body">
            <h5 class="card-title">Counter Servlet</h5>
            <p class="card-text">Created a counter servlet that increases whenever visited.</p>
            <a href="/count" class="btn btn-primary">Go to project</a>
          </div>
        </div>

        <div class="card col-6" style="width: 18rem;">
          <div class="card-body">
            <h5 class="card-title">Counter Servlet</h5>
            <p class="card-text">Created a new counter servlet that has more included</p>
            <a href="counter.jsp" class="btn btn-primary">Go to project</a>
          </div>
        </div>

      </div>

    </div>


    <br>
      <br>
<%--    <a href="/products">Want to see my product wishlist</a>--%>
  </center>

  
<%--  footer included--%>
  <%@include file="partials/footer.jsp"%>
  </body>
</html>
