<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/20/20
  Time: 1:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP Lesson</title>
</head>
<body>

    <h3>JSP Expression</h3>
    <p>
        Convert a string to uppercase
        <%= new String("I'm not yelling!").toUpperCase()%>
    </p>

    <br>

    <p>25 multiply by 4
        <%= 25 * 4 %>
    </p>

    <br>

    <p>
        Is 75 less than 65
        <%= 75 < 65 %>
    </p>

    <h3>JSP Scriplets</h3>

    <%
        for (int i = 1; i <= 5; i++) {
            out.println("<br>" + i);
        }
    %>

    <h3>JSP Declaration</h3>

<%--    create a method --%>
    <%!
        String makeItLowerCase(String slogan) {
            return slogan.toLowerCase();
        }
    %>

<%--    call our method --%>
    <%=
        makeItLowerCase("Melts in Your Mouth, Not in Your Hands")
    %>

    <h3>Expression Language (EL) </h3>
    <%
        request.setAttribute("greeting", "Hello, Servlet!");
    %>

    <p>
        Here is the message from our EL:
    </p>

<%--    this will not work --%>
    <%
        String message = "Hello World!";
    %>
    <p>Here is the second message: ${message}</p>

</body>
</html>
