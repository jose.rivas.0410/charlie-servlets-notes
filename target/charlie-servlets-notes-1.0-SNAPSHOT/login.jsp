<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/20/20
  Time: 2:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
<%--    include bootstrap --%>
    <%@include file="partials/bootstrap.jsp"%>
</head>
<body>
<%-- navbar --%>
<%@include file="partials/navbar.jsp"%>

    <center>
        <h1>Please Login</h1>
        <div class="container">
            <form action="/login" method="post">
                <div class="form-group">
                    Username:<input type="text" id="username" name="username" placeholder="Username">
                </div>
                    <br>
                <div class="form-group">
                    Password:<input type="password" id="password" name="password" placeholder="Password">
                </div>
                    <br>
                <input type="submit" value="Login">
            </form>
        </div>
    </center>

<%@include file="partials/footer.jsp"%>
</body>
</html>
