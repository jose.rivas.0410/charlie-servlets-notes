<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/22/20
  Time: 8:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sessions and Cookies Lesson</title>
    <jsp:include page="/partials/bootstrap.jsp"/>
</head>
<body>

    <center>
        <h3>Please Login</h3>
        <div class="container">
            <form action="/admin-login" method="post">
                <div class="form-group">
                    <input type="text" id="username" name="username" placeholder="Username">
                </div>

                <div class="form-group">
                    <input type="password" id="password" name="password" placeholder="Password">
                </div>

                <input type="submit" value="Login">
            </form>
        </div>
    </center>

</body>
</html>
