<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/22/20
  Time: 8:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sessions and Cookies Lesson</title>
    <jsp:include page="/partials/bootstrap.jsp"/>
</head>
<body>

    <center>
        <h1>Viewing Profile</h1>
        <h3>Logged in successfully</h3>

        <p>Hello ${username}</p>
        <a href="/admin-logout">Logout Here</a>
    </center>

</body>
</html>
