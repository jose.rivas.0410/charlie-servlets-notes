<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/21/20
  Time: 10:55 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add A Student</title>
    <%@include file="/partials/bootstrap.jsp"%>
</head>
<body>
<%@include file="/partials/navbar.jsp"%>
    <center>
        <h3>Enter the new student's info:</h3>

        <form action="/students/add-student" method="post">
            <label for="firstName">First Name:</label>
                <input type="text" name="firstName" id="firstName">
                    <br>
            <label for="lastName">Last Name:</label>
                <input type="text" name="lastName" id="lastName">
                    <br>
            <label for="email">Email:</label>
                <input type="text" name="email" id="email">
                    <br>
            <input type="submit" value="Enter new student">
        </form>
        <a href="/students">Return back to Enrolled Students</a>
    </center>
<%@include file="/partials/footer.jsp"%>
</body>
</html>
