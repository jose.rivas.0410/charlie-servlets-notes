<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/21/20
  Time: 10:24 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Students</title>
    <%@include file="/partials/bootstrap.jsp"%>
</head>
<body>
<%@include file="/partials/navbar.jsp"%>
    <center>
        <h1>Welcome</h1>
        <h3>Here are all of the students: </h3>

            <table border="1">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                </tr>


<%--            JSTL DIRECTIVE--%>
            <c:forEach items="${listOfStudents}" var="enrolledStudents">
                <tr>
                    <td>${enrolledStudents.firstName}</td>
                    <td>${enrolledStudents.lastName}</td>
                    <td>${enrolledStudents.email}</td>
                </tr>

            </c:forEach>
        </table>
        <a href="/students/add-student">Enroll a student here</a>
    </center>
    <%@include file="/partials/footer.jsp"%>
</body>
</html>
