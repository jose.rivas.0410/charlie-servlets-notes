<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/21/20
  Time: 1:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add a new Product</title>
    <%@include file="/partials/bootstrap.jsp"%>
</head>
<body>
<%@include file="/partials/navbar.jsp"%>

    <center>
        <h3>Enter the Product's Info below:</h3>

        <form action="/products/add-product" method="post">
            <label for="name">Name:</label>
            <input type="text" name="name" id="name">
            <br>
            <label for="category">Category:</label>
            <input type="text" name="category" id="category">
            <br>
            <label for="price">Price:</label>
            <input type="text" name="price" id="price">
            <br>
            <input type="submit" value="Add new product">
        </form>
        <a href="/products">Return back to product wishlist</a>
    </center>
<%@include file="/partials/footer.jsp"%>
</body>
</html>
