<%--
  Created by IntelliJ IDEA.
  User: student66
  Date: 10/22/20
  Time: 8:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Counter</title>
    <%@include file="partials/bootstrap.jsp"%>
</head>
<body>
<%@include file="partials/navbar.jsp"%>
    <center>
        <%
            Integer hitsCount = (Integer)application.getAttribute("hitCounter");
            if( hitsCount ==null || hitsCount == 0 ) {
                /* First visit */
                out.println("Welcome to my website!");
                hitsCount = 1;
            } else {
                /* return visit */
                out.println("Welcome back to my website!");
                hitsCount += 1;
            }
            application.setAttribute("hitCounter", hitsCount);
        %>
        <center>
            <h1>Total number of visits:</h1>
            <h3><%= hitsCount%></h3>
        </center>
    </center>
<%@include file="partials/footer.jsp"%>
</body>
</html>
