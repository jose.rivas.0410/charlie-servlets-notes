package CodeBoundWishList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddProductServlet", urlPatterns = "/products/add-product")
public class AddProductServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Products productsDao = DaoFactory.getProductsDao();
        String newName = request.getParameter("name");
        String newCategory = request.getParameter("category");
        String newPrice = request.getParameter("price");
        Product newProduct = new Product(newName, newCategory, newPrice);
        productsDao.insert(newProduct);
        response.sendRedirect("/products");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/product-app/add-product.jsp").forward(request, response);
    }
}
