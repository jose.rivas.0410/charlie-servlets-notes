package CodeBoundWishList;

import java.util.ArrayList;
import java.util.List;

public class ListProduct implements Products {
    private List<Product> products= new ArrayList<>();

    public ListProduct() {
        insert(new Product("PS5", "Electronic", "$499"));
        insert(new Product("Origin PC EON15-X", "Electronic", "$1879"));
        insert(new Product("LED Light Strip", "Home", "$35"));
        insert(new Product("Karratha Upholstered Low Profile Platform Bed", "Home", "$450"));
        insert(new Product("Japanese Tachi", "Decoration", "$1390"));
        insert(new Product("Bylnice Gaming Desk", "Home", "$200"));

    }

    @Override
    public List<Product> all() {
        return this.products;
    }

    @Override
    public void insert(Product product) {
        this.products.add(product);
    }

}
