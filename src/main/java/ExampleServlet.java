import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ExampleServlet", urlPatterns = "/example")
public class ExampleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1. set the content type
        response.setContentType("text/html");

        // 2. get the PrintWriter
        PrintWriter writer = response.getWriter();

        // 3. generate the HTML content
        writer.println("<h1>Example Servlet</h1>");
        writer.println("<p>Created by Charlie Cohort</p>");
    }
}
