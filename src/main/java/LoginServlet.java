import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        boolean user1 = username.equals("admin") && password.equals("password");
        boolean user2 = username.equals("stevejobs") && password.equals("apple");
        boolean user3 = username.equals("billgates") && password.equals("microsoft");
        boolean user4 = username.equals("counting") && password.equals("countdown321");
        boolean user5 = username.equals("principal") && password.equals("wedontneednoeducation");

        if (user1) {
            request.getSession().setAttribute("user", true);
            request.getSession().setAttribute("username", username);
            response.sendRedirect("/profile");
        }else if (user2) {
            request.getSession().setAttribute("user2", true);
            request.getSession().setAttribute("username", username);
            response.sendRedirect("https://www.apple.com");
        }else if (user3) {
            request.getSession().setAttribute("user3", true);
            request.getSession().setAttribute("username", username);
            response.sendRedirect("https://www.microsoft.com");
        }else if (user4) {
            request.getSession().setAttribute("user4", true);
            request.getSession().setAttribute("username", username);
            response.sendRedirect("/counter.jsp");
        }else if (user5) {
            request.getSession().setAttribute("user4", true);
            request.getSession().setAttribute("username", username);
            response.sendRedirect("/students");
        }else {
            response.sendRedirect("/login");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        boolean isUser = false;

        if (session.getAttribute("user") != null) {

            isUser = (boolean) session.getAttribute("user");
        }

        if (isUser) {
            request.getRequestDispatcher("/profile.jsp").forward(request, response);
        }else {
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    }
}
