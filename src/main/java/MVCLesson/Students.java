package MVCLesson;

import java.util.List;

// DAO Interface
public interface Students {
    // get all the students record / data
    List<Student> all();

    // add a new student to our "database"
    void insert(Student student);
}
