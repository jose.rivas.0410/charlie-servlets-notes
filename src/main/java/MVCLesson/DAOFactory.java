package MVCLesson;

// class that provides access to our DAO
public class DAOFactory {

    // fields
    private static Students studentsDao;

    // method
    public static Students getStudentsDao() {
        if (studentsDao == null) {
            studentsDao = new ListStudents();
        }

        return studentsDao;
    }
}
