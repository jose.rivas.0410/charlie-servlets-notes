package MVCLesson;

import java.util.ArrayList;
import java.util.List;

// DAO Implementation
public class ListStudents implements Students {
    // incorporate our DAO
    private List<Student> students= new ArrayList<>();
    // created an empty list of students

    // NOTE: When an instinct of this class is created
    // we'll populate the students' info
    // - "faking" our data / database

    public ListStudents() {
        insert(new Student("Leslie", "Knope", "leslie@pawnee.com"));
        insert(new Student("Ron", "Swanson", "ron@pawnee.com"));
        insert(new Student("Ben", "Wyatt", "ben@pawnee.com"));

    }

    @Override
    public List<Student> all() {
        return this.students;
    }

    @Override
    public void insert(Student student) {
        this.students.add(student);
    }
}
