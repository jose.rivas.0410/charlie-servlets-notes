package SessionCookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AdminLogoutServlet", urlPatterns = "/admin-logout")
public class AdminLogoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // destroy the entire session object, hence logging out the user
        request.getSession().invalidate();

        // redirect the user back to admin-login.jsp
        response.sendRedirect("/admin-login");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // destroy the entire session object, hence logging out the user
        request.getSession().invalidate();

        // redirect the user back to admin-login.jsp
        response.sendRedirect("/admin-login");
    }
}
